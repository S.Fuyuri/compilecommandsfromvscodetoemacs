This is python script to FIX incompatibility of compile_commands.json from 'ue4 gen' with clang.
Inspired by https://neunerdhausen.de/posts/unreal-engine-5-with-vim/
But I just do not want to use vscode, zsh, nor vim, so I made this script to handle.

How to use:
1. do 'ue4 gen' in your unreal project path.
2. run this script in the project path, it will convert <project>/.vscode/compileCommands_${project}.json to <project>/compile_commands.json
3. Enjoy your Unreal development with proper Clang support.

Tested with: 
Gentoo Linux, Unreal 5.2.1, Clang-17.0.5, Emacs 29.1
